docker run -it --rm \
--name=mypyapp \
-e FLASK_APP=app.py \
-e FLASK_ENV=development \
-e PYTHONPATH=/home/packages \
-p 5000:5000 \
-v $PWD/packages:/home/packages \
-v $PWD/app:/home/app \
mypython \
/bin/bash
