FROM python:3.7-stretch

RUN mkdir /home/packages

COPY ./pip.conf /root/.pip/

RUN	pip3 install markupsafe

ENV PYTHONPATH=/home/packages:$PYTHONPATH
ENV PATH=/home/packages:$PATH

WORKDIR /home/app

EXPOSE 5000
