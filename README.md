# DOCKER PYTHON FOR FLASK

Un entorno docker para trabajar con Flask y Python

contenedor preparado para trabajar con Python: 3.7

Si necesitar otra version simplemente cambia la primera linea del dockerfile

FROM python:3.7-stretch

## construir la imagen si no existe

La primera vez que clonamos el repo necesitamos construir la imagen con nombre propio.

Si construimos la imagen con nombre se quedara en nuestro sistema y podremos usarla para todos los proyectos que la necesitemos

    docker build --no-cache --pull . -t mypython

## arrancar el contenedor

    docker run -it --rm --name=mypyapp -p 5000:5000 -v $PWD:/home/app mypython /bin/bash

Para no tener que escribir un comando tan largo he creado un **runDocker.sh**

    ./runDocker.sh

Cuando ejecutamos el script o el comando se nos abre un terminal y estamos dentro del contenedor en el que tenemos Python

# Ejecutamos pythom dentro del contenedor

editar el archivo requirements.txt y añadir los paquete que necesitemos

ejecutar: pip3 install -r requirements.txt para actualizarlos

se ha creado unos accesos directos con make para actualizar paquetes y correr el servidor:

    make update

    make run

## acceder a la aplicacion en el navegador

para acceder a la pagina como expone el puerto 5000

http://localhost:5000/

## Deploy automático en Heroku a través de la CI de GitLab

al fichero .gitlab-ci.yml.example quitale al extension .example

add la variable de entorno en GitLab HEROKU_PRODUCTION_API_KEY
